package ru.kpfu;

public class List {
    private Element head;

    public List() {
        head = null;
    }

    // Добавление элемента в начало списка
    public void insert(int data) {
        Element elem = new Element(data, head);
        head = elem;
    }

    // Удаление первого элемента из списка
    public void removeHead() {
        if (head != null) {
            head = head.next;
        }
    }

    public Element getHead() { return head; }

    public void insertAfter(Element cur, int data) {}
    public void removeAfter(Element cur) {}

    // Вывод списка
    public void print() {
        Element cur = head;
        while (cur != null) {
            System.out.print(cur.data + " ");
            cur = cur.next;
        }
        System.out.println("");
    }

    public void removeDup() {
        Element p = head;
        while (p != null && p.next != null) {
            if (p.data == p.next.data) {
                p.next = p.next.next;
            } else {
                p = p.next;
            }
        }
    }
}
