package ru.kpfu;

public class Main {

    public static void main(String[] args) {
	    List l = new List();

	    int[] a = {1,2,3,4,5,6};
	    for (int i=a.length-1; i>=0; i--) {
	    	l.insert(a[i]);
		}
	    l.print();

	    int pos = 2;

	    Element p = l.getHead();
	    for (int i=0; i<pos; i++) {
	    	p = p.next;
		}

	    Element q = l.getHead();
	    while (q != null && q.next != null) {
	    	q = q.next;
		}

	    // make a cycle
	    q.next = p;

	    // should print infinitely
		//l.print();
    }
}
